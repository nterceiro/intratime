package com.nterceiro.intratime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntratimeApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntratimeApplication.class, args);
    }
}
