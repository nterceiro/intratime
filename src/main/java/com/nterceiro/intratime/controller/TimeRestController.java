package com.nterceiro.intratime.controller;

import com.nterceiro.intratime.model.StampType;
import com.nterceiro.intratime.model.TimeStamp;
import com.nterceiro.intratime.model.TimeStamps;
import com.nterceiro.intratime.service.TimeStampService;
import com.nterceiro.intratime.util.ElapsedTimeUtils;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest")
@AllArgsConstructor
public class TimeRestController {

    private final TimeStampService timeStampService;

    @GetMapping("/status")
    public StampType status() {
        return timeStampService.getCurrentStampType();
    }

    @GetMapping
    public TimeStamps findBy(@RequestParam(required = false) final String startDate,
        @RequestParam(required = false) final String endDate) {

        final List<TimeStamp> timestamps = timeStampService.findTimeStamp(startDate, endDate);
        return TimeStamps.builder()
            .workedTime(ElapsedTimeUtils.calculateWorkedTime(timestamps))
            .timeStampList(timestamps)
            .build();
    }

    @PostMapping
    public TimeStamp save(@RequestBody(required = false) final TimeStamp timeStamp) {
        return timeStampService.saveTimeStamp(timeStamp);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable final long id) {
        timeStampService.delete(id);
    }
}
