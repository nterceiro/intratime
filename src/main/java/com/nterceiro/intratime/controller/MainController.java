package com.nterceiro.intratime.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class MainController {

    @GetMapping("/")
    public String index(final Model model) {
        return "index";
    }

    @GetMapping("/script/{js}.js")
    public String js(final Model model, @PathVariable final String js) {
        return "script/" + js + ".js";
    }
}
