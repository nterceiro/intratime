package com.nterceiro.intratime.entity;

import com.nterceiro.intratime.model.StampType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "timestamp")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeStampEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NonNull
    private LocalDateTime dateTime;
    @NonNull
    private StampType stampType;

}
