package com.nterceiro.intratime.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeStamp {

    private long id;
    private LocalDateTime dateTime;
    private StampType stampType;

    public boolean isEmpty() {
        return this.dateTime == null || this.stampType == null;
    }
}
