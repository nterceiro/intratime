package com.nterceiro.intratime.model;

public enum StampType {
    START, END
}
