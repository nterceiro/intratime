package com.nterceiro.intratime.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ElapsedTime {

    private static final int SIXTY = 60;

    private int hours;
    private int minutes;
    private int seconds;

    public void increaseSeconds(final long seconds) {
        final long remainingSeconds = seconds % SIXTY;
        final long minutes = seconds / SIXTY;
        final long remainingMinutes = minutes % SIXTY;
        final long hours = minutes / SIXTY;

        this.hours += hours;
        this.minutes += remainingMinutes;
        this.seconds += remainingSeconds;
    }
}
