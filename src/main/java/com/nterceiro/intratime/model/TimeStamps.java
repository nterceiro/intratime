package com.nterceiro.intratime.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeStamps {

    private ElapsedTime workedTime;
    private List<TimeStamp> timeStampList;
}
