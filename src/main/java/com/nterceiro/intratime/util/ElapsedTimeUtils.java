package com.nterceiro.intratime.util;

import com.nterceiro.intratime.model.ElapsedTime;
import com.nterceiro.intratime.model.TimeStamp;
import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static com.nterceiro.intratime.model.StampType.START;

@UtilityClass
public class ElapsedTimeUtils {

    public static ElapsedTime calculateWorkedTime(final List<TimeStamp> timeStamps) {
        final ElapsedTime workedTime = ElapsedTime.builder().build();

        for (int t = 0; t < timeStamps.size(); t++) {
            final TimeStamp currentTimeStamp = timeStamps.get(t);
            if (currentTimeStamp.getStampType() == START) {
                t = nextEndPosition(timeStamps, t);
                final long totalSeconds = getElapsedSeconds(currentTimeStamp, getOptionalTimeStamp(timeStamps, t));
                workedTime.increaseSeconds(totalSeconds);
            }
        }

        return workedTime;
    }

    private static int nextEndPosition(final List<TimeStamp> timeStamps, final Integer currentIndex) {
        int nextIndex = currentIndex + 1;
        Optional<TimeStamp> nextTimeStamp = getOptionalTimeStamp(timeStamps, nextIndex);
        while (nextTimeStamp.isPresent() && nextTimeStamp.get().getStampType() == START) {
            nextIndex++;
            nextTimeStamp = getOptionalTimeStamp(timeStamps, nextIndex);
        }

        return nextIndex;
    }

    private static Optional<TimeStamp> getOptionalTimeStamp(final List<TimeStamp> timeStamps, final int index) {
        return Optional.ofNullable(index < timeStamps.size() ? timeStamps.get(index) : null);
    }

    private static long getElapsedSeconds(final TimeStamp start, final Optional<TimeStamp> end) {
        if (end.isEmpty()) {
            return 0;
        }

        final LocalDateTime startTime = start.getDateTime();
        final LocalDateTime endTime = end.get().getDateTime();
        return ChronoUnit.SECONDS.between(startTime, endTime);
    }
}
