package com.nterceiro.intratime.repository;

import com.nterceiro.intratime.entity.TimeStampEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface TimeStampRepository extends CrudRepository<TimeStampEntity, Long> {

    @Override
    List<TimeStampEntity> findAll();

    List<TimeStampEntity> findByDateTimeGreaterThanEqualAndDateTimeLessThanEqualOrderByDateTimeAsc(LocalDateTime startDateTime,
        LocalDateTime endDateTime);

    Optional<TimeStampEntity> findFirstByOrderByDateTimeDesc();
}
