package com.nterceiro.intratime.mapper;

import com.nterceiro.intratime.entity.TimeStampEntity;
import com.nterceiro.intratime.model.TimeStamp;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

import java.util.List;

@Mapper
public interface TimeStampMapper {

    TimeStampEntity toEntity(TimeStamp entity);

    TimeStamp toDto(TimeStampEntity entity);

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    List<TimeStamp> toDtos(List<TimeStampEntity> entity);
}
