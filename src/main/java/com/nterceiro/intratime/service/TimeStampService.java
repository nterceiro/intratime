package com.nterceiro.intratime.service;

import com.nterceiro.intratime.entity.TimeStampEntity;
import com.nterceiro.intratime.mapper.TimeStampMapper;
import com.nterceiro.intratime.model.StampType;
import com.nterceiro.intratime.model.TimeStamp;
import com.nterceiro.intratime.repository.TimeStampRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.nterceiro.intratime.model.StampType.END;
import static com.nterceiro.intratime.model.StampType.START;

@Service
@AllArgsConstructor
public class TimeStampService {

    private static final LocalTime START_TIME = LocalTime.of(0, 0, 0);
    private static final LocalTime END_TIME = LocalTime.of(23, 59, 59);
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private final TimeStampRepository timeStampRepository;
    private final TimeStampMapper timeStampMapper;

    public StampType getCurrentStampType() {
        return timeStampRepository.findFirstByOrderByDateTimeDesc()
            .map(TimeStampEntity::getStampType)
            .orElse(END);
    }

    public List<TimeStamp> findTimeStamp(final String startDate, final String endDate) {
        final LocalDateTime start = StringUtils.hasLength(startDate)
            ? LocalDateTime.of(LocalDate.parse(startDate, DATE_TIME_FORMATTER), START_TIME)
            : LocalDateTime.of(LocalDate.now().minusWeeks(1), START_TIME);
        final LocalDateTime end = StringUtils.hasLength(endDate)
            ? LocalDateTime.of(LocalDate.parse(endDate, DATE_TIME_FORMATTER), END_TIME)
            : LocalDateTime.of(LocalDate.now(), END_TIME);

        return timeStampMapper.toDtos(timeStampRepository
            .findByDateTimeGreaterThanEqualAndDateTimeLessThanEqualOrderByDateTimeAsc(start, end));
    }

    public TimeStamp saveTimeStamp(final TimeStamp requestedTimeStamp) {
        final TimeStamp timeStamp = (requestedTimeStamp == null || requestedTimeStamp.isEmpty())
            ? buildDefaultTimeStamp() : requestedTimeStamp;
        return timeStampMapper.toDto(timeStampRepository.save(timeStampMapper.toEntity(timeStamp)));
    }

    public void delete(final long id) {
        timeStampRepository.deleteById(id);
    }

    private TimeStamp buildDefaultTimeStamp() {
        return TimeStamp.builder()
            .stampType(getNextStampType(getCurrentStampType()))
            .dateTime(LocalDateTime.now())
            .build();
    }

    private static StampType getNextStampType(final StampType stampType) {
        return stampType == START ? END : START;
    }
}
