const disableProperty = "disabled";
const btnSuccessProperty = "btn-success";
const inButton = "#in";
const outButton = "#out";
const customDateTimePickerForm = "#customDateTimePicker";
const addInButton = "#addIn";
const addOutButton = "#addOut";
const getInCalendar = "#getIn";
const getOutCalendar = "#getOut";
const totalTimeDiv = "#totalTime";
const dateTimePicker = "DateTimePicker";
const stampListTable = "#stampList";
const deleteButton = "#delete";
const datePickerChange = "dp.change";
const endType = "END";
const startType = "START";
const dateFormat = "DD/MM/YYYY";
const rowInit = "<tr>";
const rowEnd = "</tr>";
const columnInit = "<td>";
const columnEnd = "</td>";
const deleteButtonInit = "<button type=\"button\" class=\"btn btn-danger\" id=\"delete";
const deleteButtonEnd = "\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
const restEndpoint = "/rest";
const restStatusEndpoint = "/rest/status";

$(document).ready(function() {
    initStampButtons();
    initForm();
    initTotalTimeStamps();
})

function initStampButtons() {
    disableButton(inButton);
    disableButton(outButton);
    loadCurrentStatus();
    $(outButton).click(function() {
        stampNow(inButton, outButton)
    });
    $(inButton).click(function() {
        stampNow(outButton, inButton)
    });
}

function disableButton(button) {
    $(button).prop(disableProperty, true)
    $(button).removeClass(btnSuccessProperty);
}

function enableButton(button) {
    $(button).prop(disableProperty, false)
    $(button).addClass(btnSuccessProperty);
}

function loadCurrentStatus() {
    $.get(restStatusEndpoint, function(data, status) {
        if (data == endType) {
            enableButton(inButton)
            disableButton(outButton)
        } else {
            enableButton(outButton)
            disableButton(inButton)
        }
    })
}

function stampNow(enabledButton, disabledButton) {
    $.post(restEndpoint, function(data, status) {
        console.log(data.id);
        enableButton(enabledButton);
        disableButton(disabledButton);
        loadTimeStamps();
    });
}

function initForm() {
    $(customDateTimePickerForm).datetimepicker({
        inline: true,
        sideBySide: true
    });

    $(addInButton).click(function() {
        stampCustom(buildTimestamp(startType));
    });
    $(addOutButton).click(function() {
        stampCustom(buildTimestamp(endType));
    });
}

function buildTimestamp(type) {
    var date = formatPostDate(new Date($(customDateTimePickerForm).data(dateTimePicker).date()));
    return "{\"dateTime\":\"" + date + "\",\"stampType\":\"" + type + "\"}"
}

function formatPostDate(date) {
    var day = withZero(date.getDate());
    var month = withZero(date.getMonth() + 1);
    var year = date.getFullYear();
    var hours = withZero(date.getHours());
    var minutes = withZero(date.getMinutes());
    return year + "-" + month + "-" + day + "T" + hours + ":" + minutes + ":00.000";
}

function withZero(data) {
    return data < 10 ? "0" + data : data;
}

function stampCustom(timestamp) {
    $.ajax({
        url: restEndpoint,
        type: "POST",
        data: timestamp,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data, status) {
            console.log(data.id);
            loadCurrentStatus();
            loadTimeStamps();
        }
    })
}

function initTotalTimeStamps() {
    buildTotalTimeText();
    $(getInCalendar).datetimepicker({
        format: dateFormat,
        date: new Date()
    });
    $(getOutCalendar).datetimepicker({
        format: dateFormat,
        date: new Date()
    });
    loadTimeStamps();
    $(getInCalendar).on(datePickerChange, function (e) {
        var inDate = e.date._d;
        var outDate = $(getOutCalendar).data(dateTimePicker).date()._d;
        if (inDate > outDate) {
            $(getOutCalendar).data(dateTimePicker).date(e.date);
        }
        loadTimeStamps();
    });
    $(getOutCalendar).on(datePickerChange, function (e) {
        var inDate = $(getInCalendar).data(dateTimePicker).date()._d;
        var outDate = e.date._d;
        if (inDate > outDate) {
            $(getInCalendar).data(dateTimePicker).date(e.date);
        }
        loadTimeStamps();
    });
}

function loadTimeStamps() {
    $.get(restEndpoint + buildDateQueryParams(), function(data, status) {
        buildTotalTimeText(data.workedTime);
        buildStampTable(data)
    })
}

function buildTotalTimeText(workedTime) {
    if (workedTime == null) {
        workedTime = {hours:0, minutes:0, seconds:0}
    }
    $(totalTimeDiv).html("Total horas: <b>" + workedTime.hours + "h, " + workedTime.minutes + "m, " + workedTime.seconds + "s</b>");
}

function buildDateQueryParams() {
    var startDate = formatGetDate($(getInCalendar).data(dateTimePicker).date()._d);
    var endDate = formatGetDate($(getOutCalendar).data(dateTimePicker).date()._d);
    return "?startDate=" + startDate + "&endDate=" + endDate;
}

function formatGetDate(date) {
    var day = withZero(date.getDate());
    var month = withZero(date.getMonth() + 1);
    var year = date.getFullYear();
    return day + "/" + month + "/" + year;
}

function buildStampTable(data) {
    const list = data.timeStampList;
    var htmlContent = "<thead><tr><th>Tipo</th><th>Fecha</th><th></th></tr></thead><tbody>";
    if (list.length > 0) {
        for (i = 0; i < list.length; i++) {
            var currentElement = list[i];
            htmlContent += rowInit
                + columnInit + showText(currentElement.stampType) + columnEnd
                + columnInit + formatTableDate(currentElement.dateTime) + columnEnd
                + columnInit + deleteButtonInit + currentElement.id + deleteButtonEnd + columnEnd
                + rowEnd;
        }
    }
    else {
        htmlContent += "<tr><td colspan=\"3\">No hay registros en esta fecha</td></tr>"
    }
    htmlContent += "</tbody>"
    $(stampListTable).html(htmlContent);
    addDeleteClick(list);
}

function showText(type) {
    return type == startType ? "Entrada" : "Salida";
}

function formatTableDate(stringDate) {
    const date = new Date(stringDate);
	const dayOfWeek = parseDayOfWeek(date.getDay());
	const month = parseMonth(date.getMonth());
    const hours = withZero(date.getHours());
    const minutes = withZero(date.getMinutes());
    const seconds = withZero(date.getSeconds());

    return dayOfWeek + " " + date.getDate() + " de " + month + " de " + date.getFullYear()
        + " a las " + hours + ":" + minutes + ":" + seconds;
}

function parseDayOfWeek(i) {
	switch(i) {
    	case 1: return "Lunes";
    	case 2: return "Martes";
    	case 3: return "Miércoles";
    	case 4: return "Jueves";
    	case 5: return "Viernes";
    	case 6: return "Sábado";
    	default: return "Domingo";
    }
}

function parseMonth(i) {
	switch(i) {
    	case 0: return "Enero";
    	case 1: return "Febrero";
    	case 2: return "Marzo";
    	case 3: return "Abril";
    	case 4: return "Mayo";
    	case 5: return "Junio";
    	case 6: return "Julio";
    	case 7: return "Agosto";
    	case 8: return "Septiembre";
    	case 9: return "Octubre";
    	case 10: return "Noviembre";
    	default: return "Diciembre";
    }
}

function addDeleteClick(list) {
    for (i = 0; i < list.length; i++) {
        var id = list[i].id;
        $(deleteButton + id).click(function() {
            var buttonId = $(this).attr("id").substring("6")
            deleteRegistry(buttonId);
        });
    }
}

function deleteRegistry(id) {
    $.ajax({
        url: restEndpoint + "/" + id,
        type: "DELETE",
        contentType: "application/json; charset=utf-8",
        success: function(data, status) {
            loadCurrentStatus();
            loadTimeStamps();
        }
    })
}