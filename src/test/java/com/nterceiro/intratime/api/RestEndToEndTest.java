package com.nterceiro.intratime.api;

import com.nterceiro.intratime.model.StampType;
import com.nterceiro.intratime.model.TimeStamp;
import com.nterceiro.intratime.model.TimeStamps;
import com.nterceiro.intratime.repository.TimeStampRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.StringUtils;

import java.net.URI;
import java.time.LocalDateTime;

import static com.nterceiro.intratime.model.StampType.END;
import static com.nterceiro.intratime.model.StampType.START;
import static com.nterceiro.intratime.util.TimeStampUtils.buildTimeStampEntity;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class RestEndToEndTest {

    private static final String TIME_STAMP = "{\"dateTime\":\"2023-03-28T11:02:25.0000000\",\"stampType\":\"END\"}";

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TimeStampRepository repository;

    @LocalServerPort
    private int port;

    @AfterEach
    private void tearDown() {
        repository.deleteAll();
    }

    @Test
    void getStatus() {
        final RequestEntity<Void> request = new RequestEntity<>(buildHeaders(), HttpMethod.GET, buildURI("/status"));

        final ResponseEntity<StampType> response = restTemplate.exchange(request, StampType.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        final StampType stampType = response.getBody();
        assertEquals(END, stampType);
    }

    @ParameterizedTest(name = "get for request params {0}")
    @CsvSource(value = {"null", "?startDate=28/03/2023", "?endDate=28/03/2023"}, nullValues = "null")
    void getTimeStamps(final String queryParam) {
        final RequestEntity<Void> request = new RequestEntity<>(buildHeaders(), HttpMethod.GET, buildURI(queryParam));

        final ResponseEntity<TimeStamps> response = restTemplate.exchange(request, TimeStamps.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        final TimeStamps timeStamps = response.getBody();
        assertTrue(timeStamps.getTimeStampList().isEmpty());
    }

    @Test
    void saveTimeStamp() {
        final RequestEntity<String> request = new RequestEntity<>(TIME_STAMP, buildHeaders(), HttpMethod.POST, buildURI(null));

        final ResponseEntity<TimeStamp> response = restTemplate.exchange(request, TimeStamp.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        final TimeStamp timeStamp = response.getBody();
        assertAll(
            () -> assertEquals(END, timeStamp.getStampType()),
            () -> assertEquals(LocalDateTime.of(2023, 3, 28, 11, 2, 25), timeStamp.getDateTime())
        );
    }

    @Test
    void saveTimeStampNow() {
        final RequestEntity<String> request = new RequestEntity<>(null, buildHeaders(), HttpMethod.POST, buildURI(null));

        final ResponseEntity<TimeStamp> response = restTemplate.exchange(request, TimeStamp.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        final TimeStamp timeStamp = response.getBody();
        assertAll(
            () -> assertEquals(START, timeStamp.getStampType()),
            () -> assertNotNull(timeStamp.getDateTime())
        );
    }

    @Test
    void delete() {
        long id = repository.save(buildTimeStampEntity(START, LocalDateTime.now())).getId();
        final RequestEntity<Void> request = new RequestEntity<>(buildHeaders(), HttpMethod.DELETE, buildURI("/" + id));

        final ResponseEntity<Void> response = restTemplate.exchange(request, Void.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private URI buildURI(final String extraUrl) {
        final StringBuilder stringBuilder = new StringBuilder("http://localhost:" + port + "/rest");
        if (StringUtils.hasLength(extraUrl)) {
            stringBuilder.append(extraUrl);
        }
        return URI.create(stringBuilder.toString());
    }

    private static HttpHeaders buildHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return headers;
    }
}
