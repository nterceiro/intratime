package com.nterceiro.intratime.integration.service;

import com.nterceiro.intratime.model.TimeStamp;
import com.nterceiro.intratime.repository.TimeStampRepository;
import com.nterceiro.intratime.service.TimeStampService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.nterceiro.intratime.model.StampType.END;
import static com.nterceiro.intratime.model.StampType.START;
import static com.nterceiro.intratime.util.TimeStampUtils.buildTimeStamp;
import static com.nterceiro.intratime.util.TimeStampUtils.buildTimeStampEntity;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class TimeStampServiceTest {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private static final LocalDateTime DATE_TIME = LocalDateTime.of(2023, 1, 2, 12, 40, 0);
    private static final String DAY_BEFORE = DATE_TIME_FORMATTER.format(DATE_TIME.minusDays(1));
    private static final String DAY_AFTER = DATE_TIME_FORMATTER.format(DATE_TIME.plusDays(1));

    @Autowired
    private TimeStampRepository repository;
    @Autowired
    private TimeStampService service;

    @AfterEach
    private void tearDown() {
        repository.deleteAll();
    }

    @Test
    void findByDate() {
        repository.save(buildTimeStampEntity(START, DATE_TIME));
        repository.save(buildTimeStampEntity(END, DATE_TIME.plusHours(2)));

        final List<TimeStamp> list = service.findTimeStamp(DAY_BEFORE, DAY_AFTER);

        assertAll(
            () -> assertEquals(2, list.size()),
            () -> {
                final TimeStamp timeStamp = list.get(0);
                assertTrue(timeStamp.getId() >0L);
                assertEquals(START, timeStamp.getStampType());
                assertEquals(DATE_TIME, timeStamp.getDateTime());
            },
            () -> {
                final TimeStamp timeStamp = list.get(1);
                assertTrue(timeStamp.getId() > 0L);
                assertEquals(END, timeStamp.getStampType());
                assertEquals(DATE_TIME.plusHours(2), timeStamp.getDateTime());
            }
        );
    }

    @Test
    void saveAndDelete() {
        final TimeStamp response = service.saveTimeStamp(buildTimeStamp(END, DATE_TIME));

        assertAll(
            () -> assertTrue(response.getId() > 0L),
            () -> assertEquals(END, response.getStampType()),
            () -> assertEquals(DATE_TIME, response.getDateTime()),
            () -> assertFalse(repository.findAll().isEmpty())
        );

        service.delete(response.getId());

        assertTrue(repository.findAll().isEmpty());
    }

    @Test
    void saveEmptyAndDelete() {
        final TimeStamp response = service.saveTimeStamp(null);

        assertAll(
            () -> assertTrue(response.getId() > 0L),
            () -> assertEquals(START, response.getStampType()),
            () -> assertNotNull(response.getDateTime()),
            () -> assertFalse(repository.findAll().isEmpty())
        );

        service.delete(response.getId());

        assertTrue(repository.findAll().isEmpty());
    }
}
