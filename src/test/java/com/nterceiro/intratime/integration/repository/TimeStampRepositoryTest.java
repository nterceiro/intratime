package com.nterceiro.intratime.integration.repository;

import com.nterceiro.intratime.entity.TimeStampEntity;
import com.nterceiro.intratime.repository.TimeStampRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.nterceiro.intratime.model.StampType.END;
import static com.nterceiro.intratime.model.StampType.START;
import static com.nterceiro.intratime.util.TimeStampUtils.buildTimeStampEntity;
import static com.nterceiro.intratime.util.TimeStampUtils.contains;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class TimeStampRepositoryTest {

    private static final LocalDateTime START_DATE_TIME = LocalDateTime.of(2000, 1, 2, 12, 30, 15);
    private static final LocalDateTime END_DATE_TIME = LocalDateTime.of(2001, 2, 3, 13, 45, 20);

    @Autowired
    private TimeStampRepository repository;

    @BeforeEach
    private void setUp() {
        repository.save(buildTimeStampEntity(START, START_DATE_TIME));
        repository.save(buildTimeStampEntity(END, END_DATE_TIME));
    }

    @AfterEach
    private void tearDown() {
        repository.deleteAll();
    }

    @Test
    void basicOperations() {
        final List<TimeStampEntity> timeStampEntities = repository.findAll();

        assertNotNull(timeStampEntities);
        assertFalse(timeStampEntities.isEmpty());
        assertTrue(contains(timeStampEntities, START, START_DATE_TIME));
        assertTrue(contains(timeStampEntities, END, END_DATE_TIME));
    }

    @Test
    void findByDate() {
        final List<TimeStampEntity> betweenDate = repository
            .findByDateTimeGreaterThanEqualAndDateTimeLessThanEqualOrderByDateTimeAsc(START_DATE_TIME, END_DATE_TIME);

        assertAll(
            () -> assertFalse(betweenDate.isEmpty()),
            () -> {
                final TimeStampEntity start = betweenDate.get(0);
                assertAll(
                        () -> assertEquals(START, start.getStampType()),
                        () -> assertEquals(START_DATE_TIME, start.getDateTime())
                );
            },
            () -> {
                final TimeStampEntity end = betweenDate.get(1);
                assertAll(
                    () -> assertEquals(END, end.getStampType()),
                    () -> assertEquals(END_DATE_TIME, end.getDateTime())
                );
            }
        );
    }

    @Test
    void findFirst() {
        final Optional<TimeStampEntity> first = repository.findFirstByOrderByDateTimeDesc();

        assertTrue(first.isPresent());
        assertAll(
            () -> assertEquals(END, first.get().getStampType()),
            () -> assertEquals(END_DATE_TIME, first.get().getDateTime())
        );
    }
}
