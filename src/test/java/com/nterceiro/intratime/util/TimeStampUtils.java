package com.nterceiro.intratime.util;

import com.nterceiro.intratime.entity.TimeStampEntity;
import com.nterceiro.intratime.model.StampType;
import com.nterceiro.intratime.model.TimeStamp;
import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;
import java.util.List;

@UtilityClass
public class TimeStampUtils {

    public static TimeStampEntity buildTimeStampEntity(final StampType stampType, final LocalDateTime dateTime) {
        return TimeStampEntity.builder().stampType(stampType).dateTime(dateTime).build();
    }

    public static TimeStamp buildTimeStamp(final StampType stampType, final LocalDateTime dateTime) {
        return TimeStamp.builder().stampType(stampType).dateTime(dateTime).build();
    }

    public static boolean contains(final List<TimeStampEntity> timeStampEntities, final StampType stampType,
        final LocalDateTime dateTime) {

        return timeStampEntities.stream().anyMatch(e -> e.getStampType() == stampType && e.getDateTime().equals(dateTime));
    }
}
