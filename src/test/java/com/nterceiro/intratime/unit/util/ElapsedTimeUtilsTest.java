package com.nterceiro.intratime.unit.util;

import com.nterceiro.intratime.model.ElapsedTime;
import com.nterceiro.intratime.model.TimeStamp;
import com.nterceiro.intratime.util.ElapsedTimeUtils;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static com.nterceiro.intratime.model.StampType.END;
import static com.nterceiro.intratime.model.StampType.START;
import static com.nterceiro.intratime.util.TimeStampUtils.buildTimeStamp;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ElapsedTimeUtilsTest {

    private static LocalDateTime START_TIME = LocalDateTime.now().minusDays(2);
    private static LocalDateTime NEXT_HOUR = START_TIME.plusHours(1);
    private static LocalDateTime NEXT_TWO_HOURS = START_TIME.plusHours(2);
    private static LocalDateTime NEXT_DAY = START_TIME.plusDays(1);
    private static LocalDateTime NEXT_TWO_DAYS = NEXT_DAY.plusHours(23);
    private static final TimeStamp END_NOT_COUNT = buildTimeStamp(END, START_TIME);
    private static final TimeStamp START_1 = buildTimeStamp(START, NEXT_HOUR);
    private static final TimeStamp END_1 = buildTimeStamp(END, NEXT_TWO_HOURS);
    private static final TimeStamp END_1_NOT_COUNT = buildTimeStamp(END, NEXT_TWO_HOURS.plusMinutes(10));
    private static final TimeStamp START_2 = buildTimeStamp(START, NEXT_DAY);
    private static final TimeStamp START_2_NOT_COUNT = buildTimeStamp(START, NEXT_DAY.plusMinutes(10));
    private static final TimeStamp END_2 = buildTimeStamp(END, NEXT_DAY.plusMinutes(30).plusSeconds(15));
    private static final TimeStamp START_OPEN = buildTimeStamp(START, NEXT_TWO_DAYS);
    private static final List<TimeStamp> TIME_STAMP_LIST =
            List.of(END_NOT_COUNT, START_1, END_1, END_1_NOT_COUNT, START_2, START_2_NOT_COUNT, END_2, START_OPEN);

    @Test
    void calculateWorkHours() {
        final ElapsedTime expected = ElapsedTime.builder()
            .hours(1)
            .minutes(30)
            .seconds(15)
            .build();

        assertEquals(expected, ElapsedTimeUtils.calculateWorkedTime(TIME_STAMP_LIST));
    }
}
